import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'db-microservice',
      port: 3308,
      username: 'root',
      password: 'mysql',
      database: 'users_db',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: false,
      autoLoadEntities: false,
      migrations: [__dirname + "/migration/*.js"],
      cli: {
          migrationsDir: "migration"
      }
    }),
    UserModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
