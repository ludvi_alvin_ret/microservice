import { Controller, HttpStatus } from '@nestjs/common';
import { MessagePattern, Payload, Ctx, KafkaContext } from '@nestjs/microservices';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
    constructor(private userService: UserService) {}

    @MessagePattern('users.get')
    async getUsers(@Payload() message) {
        var data = message.value;
        const users = await this.userService.getUsers(data);
        return {
            statusCode: HttpStatus.OK,
            message: 'Success',
            result: users
        }
    }

    @MessagePattern('users.getById')
    async getUserById(@Payload() message) {
        var data = message.value;
        const users = await this.userService.getUserById(data);
        return {
            statusCode: HttpStatus.OK,
            message: 'Success',
            result: users
        }
    } 

    @MessagePattern('users.insert')
    async addUser(@Payload() message, @Ctx() context:KafkaContext) {
        var data = message.value;
        const users = await this.userService.addUser(data);

        return {
            statusCode: HttpStatus.OK,
            message: 'Success',
            result: users
        }
    }

    @MessagePattern('users.update')
    async updateUser(@Payload() message, @Ctx() context:KafkaContext) {
        const originalMessage = context.getMessage();
        const { value } = originalMessage;
        const users = await this.userService.updateUser(value);

        return {
            statusCode: HttpStatus.OK,
            message: 'Success',
            result: users
        }
    }

    @MessagePattern('users.delete')
    async deleteUser(@Payload() message, @Ctx() context:KafkaContext) {
        const originalMessage = context.getMessage();
        const { value } = originalMessage;
        const users = await this.userService.deleteUser(value);
        return {
            statusCode: HttpStatus.OK,
            message: 'Success',
            result: users
        }
    } 

    @MessagePattern('users.check.login')
    async checkUser(@Payload() message) {
        var data = message.value;
        return await this.userService.checkUser(data);
    }

    @MessagePattern('users.check.register')
    async checkUserRegister(@Payload() message) {
        var data = message.value;
        return await this.userService.checkUserRegister(data);
    }

    @MessagePattern('users.create')
    async createUser(@Payload() message, @Ctx() context:KafkaContext) {
        var data = message.value;
        return await this.userService.addUser(data);
    }
}
