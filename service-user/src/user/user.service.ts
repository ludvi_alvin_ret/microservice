import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, createConnection, getConnection, getRepository } from 'typeorm';
import { User, UserPermission } from './user.entity';
import { qbHelper } from '../helper/qbHelper';
import * as bcrypt from 'bcrypt';

interface Permission {
    name: string;
    isActive: boolean;
}

interface PermissionPath {
    name: object;
}

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(User)
        private usersRepository: Repository<User>,
    ) {}

    async getUsers(params) {
        const options = {
            "result": "paginated",
            "table": {
                "entity": "User",
                "alias": "a"
            },
            "select": [
                {"name": "a.id", "alias": "id"},
                {"name": "a.name", "alias": "name"},
                {"name": "a.created_date", "alias": "created_date"}
            ],
            "where": [
                {
                    "column": "a.is_deleted",
                    "value": "0"
                }
            ],
            "sortableColumns": ['id', 'name', 'created_date'],
            "searchableColumns": ['name', 'created_date'],
        }

        return await qbHelper(params, options);
    }

    async getUserById(params) {
        const options = {
            "result": "getRawOne",
            "table": {
                "entity": "User",
                "alias": "a"
            },
            "select": [
                {"name": "a.id", "alias": "id"},
                {"name": "a.name", "alias": "name"},
                {"name": "a.created_date", "alias": "created_date"}
            ],
            "where": [
                {
                    "column": "a.is_deleted",
                    "value": "0"
                },
                {
                    "column": "a.id",
                    "value": params.id
                }
            ]
        }

        return await qbHelper(params, options);   
    }

    async addUser(data) {
        const saltOrRounds = 10;
        const hashedPassword = await bcrypt.hash(data.password, saltOrRounds);

        const now = new Date();
        const created_date = now.toISOString();
        const newUser = {
            name: data.name,
            email: data.email,
            password: hashedPassword,
            created_date: created_date,
        }
        const user = this.usersRepository.create(newUser);
        return await this.usersRepository.save(newUser);
    }

    async updateUser(data) {
        createConnection().then(async connection => {
            await getConnection()         
            .createQueryBuilder() .update(User) 
            .set({ name: "Michael" }) .where("id = :id", { id: 1 }) .execute(); 
            console.log("data updated");    
        }).catch(error => console.log(error));
    }

    async deleteUser(data) {
        return await this.usersRepository.findOne(data.id);
    }

    async checkUser(params) {
        const username = params.name
        const options = {
            "result": "getRawOne",
            "table": {
                "entity": "User",
                "alias": "a"
            },
            "select": [
                {"name": "a.id", "alias": "id"},
                {"name": "a.user_group_id", "alias": "user_group_id"},
                {"name": "a.name", "alias": "name"},
                {"name": "a.password", "alias": "password"},
            ],
            "where": [
                {
                    "column": "a.is_deleted",
                    "value": "0"
                },
                {
                    "column": "a.name",
                    "value": username
                }
            ]
        }

        const res = await qbHelper(params, options);
        
        if(res){
            const permissions = await this.getUserPermissions(res.user_group_id)
            const role = await this.getUserRole(res.user_group_id)
            
            const user = {
                id: res.id,
                userGroupId: res.user_group_id,
                username: res.name,
                password: res.password,
                profile: '',
                role: role,
                permissionsB: permissions.backend,
                permissionsF: permissions.frontend,
                permissionsList: permissions.list
            }

            return user
        }else{
            return null
        } 
    }

    async checkUserRegister(params) {
        const username = params.name
        const email = params.email
        const options = {
            "result": "getRawOne",
            "table": {
                "entity": "User",
                "alias": "a"
            },
            "select": [
                {"name": "a.id", "alias": "id"},
                {"name": "a.name", "alias": "name"},
                {"name": "a.password", "alias": "password"},
            ],
            "where": [
                {
                    "column": "a.is_deleted",
                    "value": "0"
                },
                {
                    "column": "a.name",
                    "value": username
                },
                {
                    "column": "a.email",
                    "value": email
                }
            ]
        }

        const res = await qbHelper(params, options);
        if(res){
            const user = {
                id: res.id,
                name: res.name
            }
            return user
        }else{
            return null
        } 
    }

    async getListPermission(){
        const options = {
            "result": "getRawMany",
            "table": {
                "entity": "RefPermissions",
                "alias": "a"
            },
            "select": [
                {"name": "a.id", "alias": "id"},
                {"name": "a.name", "alias": "name"},
            ],
            "where": [
                {
                    "column": "a.is_deleted",
                    "value": 0
                }
            ]
        }

        const res = await qbHelper({}, options);
        return res
    }

    async getUserPermissions(userGroupId) {
        const refPermissions = await this.getListPermission()

        const arrPath: PermissionPath[] = [];
        let indexedArray: {[key: string]: object} = {}
        let arrList = []
        for(const index of refPermissions){
            const options = {
                "result": "getRawMany",
                "table": {
                    "entity": "UserPermission",
                    "alias": "a"
                },
                "select": [
                    {"name": "a.path", "alias": "path"},
                    {"name": "a.action", "alias": "action"},
                    {"name": "a.is_active", "alias": "is_active"},
                ],
                "where": [
                    {
                        "column": "a.user_group_id",
                        "value": userGroupId
                    },
                    {
                        "column": "a.permission_id",
                        "value": index.id
                    }
                ]
            }
    
            const resPer = await qbHelper({}, options);
            if(resPer){
                const namee = index.name

                let arrayB: {[key: string]: boolean} = {}
                for(const mod of resPer){
                    var accessPath = false
                    if(mod.is_active == 1){
                        accessPath = true

                        const mmm = mod.action+':'+mod.path
                        arrList.push(mmm)
                    }
                    arrayB[mod.action] = accessPath
                }    

                indexedArray[namee] = arrayB
            }
        }

        //console.log(indexedArray)

        const options = {
            "result": "getRawMany",
            "table": {
                "entity": "UserPermission",
                "alias": "a"
            },
            "select": [
                {"name": "a.path", "alias": "path"},
                {"name": "a.action", "alias": "action"},
                {"name": "a.is_active", "alias": "is_active"},
            ],
            "where": [
                {
                    "column": "a.user_group_id",
                    "value": userGroupId
                }
            ]
        }

        const res = await qbHelper({}, options);

        return {
            frontend: indexedArray,
            backend: res,
            list: arrList
        }
    }

    async getUserRole(userGroupId) {
        const options = {
            "result": "getRawOne",
            "table": {
                "entity": "UsersGroup",
                "alias": "a"
            },
            "select": [
                {"name": "a.name", "alias": "name"},
            ],
            "where": [
                {
                    "column": "a.id",
                    "value": userGroupId
                }
            ]
        }

        const res = await qbHelper({}, options);

        return res.name
    }
}
