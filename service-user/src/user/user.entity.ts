import { Entity, Column, PrimaryGeneratedColumn, PrimaryColumn } from 'typeorm';

@Entity('user')
export class User {
    @PrimaryGeneratedColumn()
    id: number; 

    @Column({ default: '2' })
    user_group_id: number;

    @Column({ default: null })
    name: string;

    @Column({ default: null })
    email: string;

    @Column({ default: null })
    password: string; 

    @Column({ type: 'datetime', default: null })
    created_date: string;

    @Column({ type: 'datetime', default: null })
    modified_date: string; 

    @Column({ default: '0' })
    is_deleted: number; 
}

@Entity('user_group')
export class UsersGroup {
    @PrimaryGeneratedColumn()
    id: number; 

    @Column()
    name: string;

    @Column({ type: 'datetime', default: null })
    created_date: string;

    @Column({ type: 'datetime', default: null })
    modified_date: string; 

    @Column({ default: '0' })
    is_deleted: number; 
}

@Entity('user_permissions')
export class UserPermission {
    @PrimaryColumn()
    id: number; 

    @Column()
    user_group_id: number; 

    @Column()
    path: string;

    @Column()
    action: string;

    @Column({ type: 'datetime', default: null })
    created_date: string;

    @Column({ type: 'datetime', default: null })
    modified_date: string; 
}

@Entity('ref_permissions')
export class RefPermissions {
    @PrimaryColumn()
    id: number; 

    @Column()
    name: string;

    @Column({ type: 'datetime', default: null })
    created_date: string;

    @Column({ type: 'datetime', default: null })
    modified_date: string; 

    @Column({ default: '0' })
    is_deleted: number; 
}