import {MigrationInterface, QueryRunner, Table} from "typeorm";

export class createTable1654084058622 implements MigrationInterface {
    private tableUser = new Table({
        name: 'user2',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true, // Auto-increment
            generationStrategy: 'increment',
          },
          {
            name: 'user_group_id',
            type: 'integer',
            isPrimary: false,
            isNullable: false,
            default: 2,
          },
          {
            name: 'name',
            type: 'varchar',
            length: '255',
            isNullable: false,
          },
          {
            name: 'email',
            type: 'varchar',
            length: '255',
            isNullable: false,
          },
          {
            name: 'password',
            type: 'varchar',
            length: '255',
            isNullable: false,
          },
          {
            name: 'created_date',
            type: 'datetime',
            isNullable: true,
            default: null,
          },
          {
            name: 'modified_date',
            type: 'datetime',
            isNullable: true,
            default: null,
          },
          {
            name: 'is_deleted',
            type: 'tinyint',
            default: 0,
          },
        ],
    });

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.createTable(this.tableUser);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropTable(this.tableUser);
    }

}
