# README #

Nest JS Microservice with Kafka Message Broker.

### How do I get set up? ###

* CD to root folder
* docker-compose up -d

### Run with rebuild image ###

* CD to root folder
* docker-compose up -d --build

### Rebuild local image all services ###

* CD to root folder
* docker-compose build --no-cache

### Remove all running container ###

* CD to root folder
* docker-compose down

### Setting url kafka ###

* If nest js run on host, kafka url "localhost:29092"
* If nest js run on docker, kafka url "kafka:9093"

### Api Docs Swagger url ###

* http://ip_address:port/docs

### Migration Database ###

* CD to service_name
* migration up, type in terminal, yarn typeorm migration:run
* migration down, type in terminal, yarn typeorm migration:revert