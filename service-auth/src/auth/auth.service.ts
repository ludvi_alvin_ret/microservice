import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
import { Auth } from './auth.entity';
import { qbHelper } from '../helper/qbHelper';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(Auth)
        private authRepository: Repository<Auth>,
    ) {} 

    async checkUserId(userId) {
        const options = {
            "result": "getRawOne",
            "table": {
                "entity": "Auth",
                "alias": "a"
            },
            "select": [
                {"name": "a.id", "alias": "id"},
                {"name": "a.refresh_token", "alias": "refresh_token"},
            ],
            "where": [
                {
                    "column": "a.is_deleted",
                    "value": "0"
                },
                {
                    "column": "a.user_id",
                    "value": userId
                }
            ]
        }

        const res =  await qbHelper({}, options);
        if(res) {
            const tokenUser = {
                id: res.id,
                refresh_token: res.refresh_token,
            }
            return tokenUser
        }else {
            return null
        } 
    }    

    async checkToken(params) {
        const token = params.token
        const options = {
            "result": "getRawOne",
            "table": {
                "entity": "Auth",
                "alias": "a"
            },
            "select": [
                {"name": "a.id", "alias": "id"},
            ],
            "where": [
                {
                    "column": "a.is_deleted",
                    "value": "0"
                },
                {
                    "column": "a.token",
                    "value": token
                }
            ]
        }

        const res = await qbHelper(params, options);
        if(res) {
            const tokenUser = {
                id: res.id,
            }
            return tokenUser
        }else {
            return null
        } 
    }

    async checkRefreshToken(params) {
        const refreshToken = params.refreshToken
        const userId = params.userId

        const options = {
            "result": "getRawOne",
            "table": {
                "entity": "Auth",
                "alias": "a"
            },
            "select": [
                {"name": "a.id", "alias": "id"},
            ],
            "where": [
                {
                    "column": "a.is_deleted",
                    "value": "0"
                },
                {
                    "column": "a.token",
                    "value": refreshToken
                }
            ]
        }

        const res = await qbHelper(params, options);
        if(res) {
            const tokenUser = {
                id: res.id,
            }
            return tokenUser
        }else {
            return null
        } 
    }

    async logToken(data) {
        const options = {
            "result": "getRawOne",
            "table": {
                "entity": "Auth",
                "alias": "a"
            },
            "select": [
                {"name": "a.id", "alias": "id"},
                {"name": "a.refresh_token", "alias": "refresh_token"},
            ],
            "where": [
                {
                    "column": "a.is_deleted",
                    "value": "0"
                },
                {
                    "column": "a.user_id",
                    "value": data.user_id
                }
            ]
        }

        const res = await qbHelper({}, options);

        if(res) {
            const now = new Date();
            const created_date = now.toISOString();
            const hashedToken = await bcrypt.hash(data.token, 10);
            const hashedRefreshToken = await bcrypt.hash(data.refresh_token, 10);
            const dataLogToken = {
                token: hashedToken,
                refresh_token: hashedRefreshToken,
                modified_date: created_date,
            }
            
            const connection = getConnection();
            const queryBuilder = await connection.createQueryBuilder() 
            queryBuilder.update(Auth) 
            .set(dataLogToken) 
            .where("id = "+res.id) 
            .execute(); 

            return queryBuilder
        }else{
            const now = new Date();
            const created_date = now.toISOString();
            const hashedToken = await bcrypt.hash(data.token, 10);
            const hashedRefreshToken = await bcrypt.hash(data.refresh_token, 10);
            const dataLogToken = {
                user_id: data.user_id,
                token: hashedToken,
                refresh_token: hashedRefreshToken,
                created_date: created_date,
                modified_date: null,
            }
            const user = this.authRepository.create(dataLogToken);
            return await this.authRepository.save(dataLogToken);
        }  
    }

    async removeToken(data) {
        const userId = data.userId;
        const now = new Date();
        const modified_date = now.toISOString();

        const dataLogToken = {
            token: '',
            refresh_token: '',
            modified_date: modified_date,
        }
        
        const connection = getConnection();
        const queryBuilder = await connection.createQueryBuilder() 
        queryBuilder.update(Auth) 
        .set(dataLogToken) 
        .where("user_id = "+userId) 
        .execute(); 

        return queryBuilder
    }    
}
