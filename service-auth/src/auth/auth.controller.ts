import { Controller } from '@nestjs/common';
import { MessagePattern, EventPattern, Payload } from '@nestjs/microservices';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) {}

    @MessagePattern('auth.check.token')
    async checkToken(@Payload() message) {
        var data = message.value;
        return await this.authService.checkToken(data);
    }

    @MessagePattern('auth.check.userId')
    async checkRefreshToken(@Payload() message) {
        var data = message.value;
        return await this.authService.checkUserId(data);
    }

    @MessagePattern('auth.log.token')
    async logToken(@Payload() message) {
        var data = message.value;
        return await this.authService.logToken(data);
    }

    @MessagePattern('auth.remove.token')
    async removeToken(@Payload() message) {
        var data = message.value;
        return await this.authService.removeToken(data);
    }
}
