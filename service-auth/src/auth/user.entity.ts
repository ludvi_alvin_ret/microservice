import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity({ database: "users_db" })
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name: string;

    @Column()
    users_group_id: number;

    @Column({ type: 'datetime', default: null })
    created_date: string;

    @Column({ type: 'datetime', default: null })
    modified_date: string; 

    @Column({ default: false })
    is_deleted: boolean; 
}