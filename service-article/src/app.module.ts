import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ArticleModule } from './article/article.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'db-microservice',
      port: 3308,
      username: 'root',
      password: 'mysql',
      database: 'articles_db',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
      autoLoadEntities: false,
    }),
    TypeOrmModule.forRoot({
      name: 'usersDB',
      type: 'mysql',
      host: 'db-microservice',
      port: 3308,
      username: 'root',
      password: 'mysql',
      database: 'users_db',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: false,
      autoLoadEntities: true,
      migrations: [__dirname + "/migration/*.js"],
      cli: {
          migrationsDir: "migration"
      }
    }),
    ArticleModule
  ],
  controllers: [],
  providers: [],
})
export class AppModule {}
