import { Injectable } from '@nestjs/common';
import { qbHelper } from '../helper/qbHelper';

@Injectable()
export class ArticleService {
    constructor() {}

    async getArticles(params) {
        const options = {
            "result": "paginated",
            "table": {
                "entity": "Article",
                "alias": "a"
            },
            "select": [
                {"name": "a.id", "alias": "id"},
                {"name": "a.title", "alias": "title"},
                {"name": "a.slug", "alias": "slug"},
                {"name": "u.name", "alias": "author_name"}
            ],
            "join": [
                {
                    "entity": "User",
                    "alias": "u",
                    "on": "u.id=a.author_id",
                    "type": "left"
                }
            ],
            "where": [
                {
                    "column": "a.is_deleted",
                    "value": "0"
                }
            ],
            "sortableColumns": ['id', 'title', 'slug'],
            "searchableColumns": ['title', 'slug', 'name'],
        }

        return await qbHelper(params, options);
    }
}
