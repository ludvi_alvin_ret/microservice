import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('article')
export class Article {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    description: string;

    @Column()
    slug: string;

    @Column()
    category_id: number;

    @Column()
    author_id: number;

    @Column({ type: 'datetime', default: null })
    created_date: string;

    @Column({ type: 'datetime', default: null })
    modified_date: string; 

    @Column({ default: false })
    is_deleted: boolean;
}

