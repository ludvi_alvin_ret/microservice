import { Controller, HttpStatus, Req } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { ArticleService } from './article.service';

@Controller('article')
export class ArticleController {
    constructor(private articleService: ArticleService) {}

    @MessagePattern('getArticles')
    async getArticles(@Payload() message) {
        var data = message.value;
        const articles = await this.articleService.getArticles(data);
        return {
            statusCode: HttpStatus.OK,
            message: 'Success',
            data: articles
        }
    }
}

