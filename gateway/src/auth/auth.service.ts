import { Injectable, Inject, HttpStatus, OnModuleInit, OnModuleDestroy, RequestTimeoutException, createParamDecorator, ExecutionContext, HttpException } from '@nestjs/common';
import { ClientKafka } from "@nestjs/microservices";
import { JwtService } from '@nestjs/jwt';
import { catchError, throwError, timeout, TimeoutError } from 'rxjs';
import * as bcrypt from 'bcrypt';
import { lastValueFrom } from 'rxjs';
import { jwtConstants } from './strategy/constants';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    @Inject('KAFKA_SERVICE') private readonly client: ClientKafka,
  ) {}
  
  async validateUser(username: string, pass: string): Promise<any> {
    const user = this.client.send('users.check.login', { name: username })
    const resp = await lastValueFrom(user);

    if (resp) {
      const isMatch = await bcrypt.compare(pass, resp.password);
      if(isMatch){
        return resp;
      }else{
        return null;
      } 
    } 

    return null;
  }

  async login(user: any) {
    const payloadToken = { 
      username: user.username, 
      sub: user.id, 
      userGroupId: user.userGroupId, 
      //acl: user.permissionsB,
      permissions: user.permissionsList
    };
    
    var expirydate = new Date();
    expirydate.setDate(expirydate.getDate() + jwtConstants.refreshTokenExpire);
    expirydate.toISOString();

    const payloadRefreshToken = { sub: user.id, expireIn: expirydate };

    const accessToken = this.jwtService.sign(payloadToken)
    const refreshAccessToken = this.jwtService.sign(payloadRefreshToken)

    const dataLogToken = {
      user_id: user.id,
      token: accessToken,
      refresh_token: refreshAccessToken
    }

    const logAuthToken = this.client.send('auth.log.token', dataLogToken)
    const respLogAuthToken = lastValueFrom(logAuthToken);

    return {
      statusCode: HttpStatus.OK,
      status: "success",
      message: "success create token",
      access_token: accessToken,
      refresh_token: refreshAccessToken,
      me: {
        profile: user.profile,
        role: user.role,
        acl: {
          modules: user.permissionsF
        }
      }
    };
  }

  async refreshToken(param) {
    const { sub, expireIn } = this.jwtService.verify(param.refresh_token);
    const userId = sub;

    const authCheckUser = await this.client.send('auth.check.userId', userId)
    const respCheckUser = await lastValueFrom(authCheckUser);

    if (respCheckUser) {
      const isRefreshTokenMatching = await bcrypt.compare(
        param.refresh_token,
        respCheckUser.refresh_token
      );
   
      if (isRefreshTokenMatching) {
        if (new Date() > new Date(expireIn)) {
          return {
            statusCode: HttpStatus.OK,
            status: "failed",
            message: "Refresh token expired"
          };
        } 

        const payloadToken = { sub: respCheckUser.id };
        const payloadRefreshToken = { sub: respCheckUser.id };

        const accessToken = this.jwtService.sign(payloadToken)
        const refreshAccessToken = this.jwtService.sign(payloadRefreshToken)

        const dataLogToken = {
          user_id: respCheckUser.id,
          token: accessToken,
          refresh_token: refreshAccessToken
        }
        
        const logAuthToken = this.client.send('auth.log.token', dataLogToken)
        const respLogAuthToken = lastValueFrom(logAuthToken);
        
        return {
          statusCode: HttpStatus.OK,
          status: "success",
          message: "success refresh token",
          access_token: accessToken,
          refresh_token: refreshAccessToken,
        };
      }else{
        return {
          statusCode: HttpStatus.OK,
          status: "failed",
          message: "Refresh token not match"
        };
      }
    }

    return {
      statusCode: HttpStatus.OK,
      status: "failed",
      message: "User with this id does not exist"
    };
  }

  async register(params) {
    const user = await this.client.send('users.check.register', params)
    const resp = await lastValueFrom(user);
    if(resp) {
      return {
        statusCode: HttpStatus.OK,
        status: "failed",
        message: "username or email already exist"
      };
    }else{
      const user = await this.client.send('users.create', params)
      const resp = await lastValueFrom(user);
      if(resp) {
        return {
          statusCode: HttpStatus.OK,
          message: 'Success',
          //result: resp
        }
      }else{
        return {
          statusCode: HttpStatus.OK,
          status: "failed",
          message: "failed create user"
        };
      }
    }
  }

  async logout(user) {
    if(user){
      const removeAuthToken = this.client.send('auth.remove.token', { userId: user.userId });
      const respRemoveAuthToken = lastValueFrom(removeAuthToken);
    }
    return {
      statusCode: HttpStatus.OK,
      status: "success",
      message: "Logout success"
    };
  }  
}