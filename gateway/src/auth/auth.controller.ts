import { Controller, Body, Post, Request, UseGuards, OnModuleInit, OnModuleDestroy, Inject, Logger  } from '@nestjs/common';
import { ClientKafka } from "@nestjs/microservices";
import { LocalAuthGuard } from './guards/local-auth.guard';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { GetUser } from './decorator/request';
import { AuthService } from './auth.service';
import {
    ApiBearerAuth,
    ApiOperation,
    ApiResponse,
    ApiTags,
} from '@nestjs/swagger';

@ApiBearerAuth()
@ApiTags('auth')  
@Controller('auth')
export class AuthController implements OnModuleInit, OnModuleDestroy {
    constructor(
        @Inject('KAFKA_SERVICE') private readonly client: ClientKafka,
        private authService: AuthService
    ) {}
  
    logger = new Logger('Main');
      
    async onModuleInit() {
        this.client.subscribeToResponseOf('auth.check.token');
        this.client.subscribeToResponseOf('auth.check.userId');
        this.client.subscribeToResponseOf('auth.log.token');
        this.client.subscribeToResponseOf('auth.remove.token');
        this.client.subscribeToResponseOf('users.check.login');
        this.client.subscribeToResponseOf('users.check.register');
        this.client.subscribeToResponseOf('users.create');
        await this.client.connect(); 
    }
  
    async onModuleDestroy() {
        await this.client.close();
    }

    @Post('/login')
    @UseGuards(LocalAuthGuard)
    async login(@Body() params, @Request() req) {
        this.logger.log('Login user');
        return this.authService.login(req.user);  
    }

    @Post('/register')
    async register(@Body() params) {
        this.logger.log('Register user');
        return this.authService.register(params); 
    }

    @Post('/refresh')
    async refreshToken(@Body() params) {
        this.logger.log('Refresh Token');
        return this.authService.refreshToken(params);  
    }

    @Post('logout')
    @UseGuards(JwtAuthGuard)
    async logout(@GetUser() user: any) {
        this.logger.log('Logout user');
        return this.authService.logout(user);  
    } 
}
