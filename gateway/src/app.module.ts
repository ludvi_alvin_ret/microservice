import { Module, CacheModule } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AppController } from './app.controller';
import { UserController } from './user/user.controller';
import { AuthController } from './auth/auth.controller';
import { jwtConstants } from './auth/strategy/constants';
import { LocalStrategy } from './auth/strategy/local.strategy';
import { JwtStrategy } from './auth/strategy/jwt.strategy';
import { JwtModule } from '@nestjs/jwt';
import { PassportModule } from '@nestjs/passport';
import { AuthService } from './auth/auth.service';
import { ArticleController } from './article/article.controller';

@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'KAFKA_SERVICE',
        transport: Transport.KAFKA,
        options: {
          client: {
            clientId: 'client',
            brokers: ['kafka:9093'],
          },
          consumer: {
            groupId: 'service-consumer',
          },
        },
      },
    ]),
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: {expiresIn: jwtConstants.tokenExpire}
    }),
    PassportModule,
    CacheModule.register({
      ttl: 10,
      max: 10,
      isGlobal: true
    })
  ],
  controllers: [
    AppController, 
    UserController, 
    AuthController, 
    ArticleController
  ],
  providers: [
    LocalStrategy, 
    JwtStrategy, 
    AuthService
    /* {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    }, */
  ],
})
export class AppModule {}
 