import { ApiProperty } from '@nestjs/swagger';

export class Article {
    @ApiProperty({ 
        example: 1, 
        description: 'user ID' 
    })
    id: number;
  
    @ApiProperty({
      example: "Article title",
      description: 'Article title',
    })
    title: string;
}