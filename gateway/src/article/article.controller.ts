import { Inject, OnModuleInit, OnModuleDestroy, Logger, Query, Controller, Get, Post, Body, Req, Put, Delete, UseGuards } from '@nestjs/common';
import { ClientKafka } from "@nestjs/microservices";
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import {
  ApiBearerAuth,
  ApiOperation,
  ApiResponse,
  ApiTags,
} from '@nestjs/swagger';
import { Article } from './article.entity';

@ApiBearerAuth()
@ApiTags('article')
@Controller('article')
export class ArticleController implements OnModuleInit, OnModuleDestroy {
    constructor(
        @Inject('KAFKA_SERVICE') private readonly client: ClientKafka,
    ) {}    
    
    logger = new Logger('Main');
    
    async onModuleInit() {
      // Need to subscribe to topic 
      this.client.subscribeToResponseOf('getArticles');
      this.client.subscribeToResponseOf('getArticlesUser');
      this.client.subscribeToResponseOf('getArticlesFilter');
      await this.client.connect();
    }

    async onModuleDestroy() {
      await this.client.close();
    }

    @Get('/')
    @ApiOperation({ summary: 'Get articles' })
    @ApiResponse({
      status: 200,
      description: 'The found record',
      type: Article,
    })
    @UseGuards(JwtAuthGuard)
    async getArticles(@Query() query) {
        this.logger.log('Get all articles');
        const pattern = 'getArticles';
        return await this.client.send(pattern, query);
    }
}
