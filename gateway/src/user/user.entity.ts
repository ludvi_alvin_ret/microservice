import { ApiProperty } from '@nestjs/swagger';

export class User {
    @ApiProperty({ 
        example: 1, 
        description: 'user ID' 
    })
    id: number;
  
    @ApiProperty({
      example: 1,
      description: 'User Group ID',
    })
    users_group_id: number;
}